# README #

## Overview ##
This is a simple shell input module for use in embedded projects, featuring extensible compile-time registeration of commands, static memory allocation, optional local echo, customizable prompt, and automatic command listing. To use this module on an embedded target, simply retarget STDIO for the serial or USB port of choice. 

## API ##

#### Types ####

###### Shell Command Type ######
```
typedef struct {
    const char* desc;
    void        (*handler)(uint8_t argc, char* argv[]);
} shell_cmd_t;
```


#### Functions ####

**void shell\_init(void)** (Re)Initializes shell state variables. This is not strictly required before using the shell module, but is helpful for resetting shell command buffer and registered commands at runtime.

**void shell\_service(void)** Service loop of the shell. Each time this function is ran, it checks for new input, parses if complete, and executes any matching commands. This service function should be called repeatedly in order to provide shell functionality. It could be called in while(1) loop as a task on an RTOS, or called on a specific period on a baremetal system. 

**int shell\_register\_cmd(shell\_cmd\_t cmd)** Registers user-created shell commands. The string description passed in *shell\_cmd\_t* is the user readable command name to be matched and listed as an available command, and the function pointer points to the function to be called upon match. Registered commands may be reset by calling *void Shell\_Init(void)*.

## License ##

Copyright (c) 2016 Trevor Wilcox wilcoxtw@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.