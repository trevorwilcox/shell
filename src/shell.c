/* Copyright (c) 2016 Trevor Wilcox wilcoxtw@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "shell.h"

//Constants

#define MAX_ARGS          15
#define MAX_LINE_LEN      80
#define MAX_CMDS          15
#define WHITESPACE        "\r\n\t "
#define DEFAULT_PROMPT    '$'
#define DEFAULT_ECHO      true
#define BUILTIN_CMD_COUNT 3

enum SHELL_STATES {
    SHELL_INPUT = 0,
    SHELL_PARSE = 1,
    SHELL_RUN   = 2,
};

//Private Function Declarations

static void get_input(void);
static void parse_input(void);
static void process_cmd(void);
static void print_help(void);
static void print_prompt(void);

static void builtin_set_prompt(uint8_t argc, char* argv[]);
static void builtin_set_local_echo(uint8_t argc, char* argv[]);
static void builtin_help(uint8_t argc, char* argv[]);

//Global Variables

static enum SHELL_STATES g_state = SHELL_INPUT;
static char              g_cmd_line[MAX_LINE_LEN + 1] = { 0 }; //+1 for NULL
static uint8_t           g_line_itr                   = 0;
static uint8_t           g_argc                       = 0;
static char*             g_argv[MAX_ARGS]             = { 0 };
static uint8_t           g_registered_cmd_cnt         = 0;
static char              g_shell_prompt               = DEFAULT_PROMPT;
static bool              g_local_echo                 = DEFAULT_ECHO;
static shell_cmd_t       g_registered_cmds[MAX_CMDS]  = { 0 };

shell_cmd_t g_builtin_cmds[BUILTIN_CMD_COUNT] = {
    {
        "help",
        builtin_help
    },
    {
        "set_prompt",
        builtin_set_prompt
    },
    {
        "set_local_echo",
        builtin_set_local_echo
    }
};

//Public Function Definitions

extern void retarget_init();

void shell_init(void)
{
    int i = 0;

    g_argc = 0;
    g_line_itr = 0;
    g_cmd_line[g_line_itr] = 0; 

    for (; i < g_registered_cmd_cnt; i++) {
        g_registered_cmds[i].desc = 0;
        g_registered_cmds[i].handler = 0;
    }

    retarget_init();

    print_prompt();
}

void shell_service(void)
{

    switch (g_state) {

        case SHELL_INPUT: get_input(); break;

        case SHELL_PARSE: parse_input(); break;

        case SHELL_RUN: process_cmd(); break;

        default: assert(0); break; // Should never happen

    }
}

int shell_register_cmd(shell_cmd_t cmd)
{
    if (g_registered_cmd_cnt < MAX_CMDS) {
        g_registered_cmds[g_registered_cmd_cnt].desc = cmd.desc;
        g_registered_cmds[g_registered_cmd_cnt].handler = cmd.handler;
        g_registered_cmd_cnt++;
    } else {
        printf("Error: %s MAX_CMDS\n", __FUNCTION__);
        return -1;
    }

    return 0;
}

//Private Function Definitions

static void builtin_set_prompt(uint8_t argc, char* argv[])
{
    if ((argc != 2) || 
        (isgraph(argv[1][0]) == 0) ||
        (strlen(argv[1]) > 1)) {

        printf("usage: set_prompt <char>\n");
        return;
    }

    g_shell_prompt = argv[1][0];
}

static void builtin_set_local_echo(uint8_t argc, char* argv[])
{
    if ((argc != 2) ||
        ((argv[1][0] != '0') && (argv[1][0] != '1')) ||
        (strlen(argv[1]) > 1)) {

        printf("usage: set_local_echo <0|1>\n");
        return;
    }

    g_local_echo = (argv[1][0] == '1') ? true : false;
    printf("\nlocal echo %s\n", g_local_echo ? "enabled" : "disabled");
}

static void builtin_help(uint8_t argc, char* argv[])
{
    print_help();
}

static void get_input(void)
{
    int new_char = -1;

    new_char = fgetc(stdin);

    //Check that a valid char was retrieved.
    if ((new_char >= 0x00) && (new_char <= 0x7F)) {

        switch (new_char) {

            case '\r':
            case '\n':
                //Handle Enter/CR/LF, process command line buffer.
                g_cmd_line[g_line_itr] = 0;
                g_line_itr = 0;
                g_state = SHELL_PARSE;

                if (g_local_echo) {
                    putchar(new_char);
                }
                break;

            case 0x7F:
            case '\b':
                //Handle backspace / delete.
                if (g_line_itr > 0) {
                    if (g_local_echo) {
                        putchar('\b');
                        putchar(' ');
                        putchar('\b');
                    }
                    g_line_itr--;
                }
                g_cmd_line[g_line_itr] = 0;
                break;

            default:
                //Append character to command line buffer, if possible.
                g_cmd_line[g_line_itr] = new_char;

                if (g_line_itr < MAX_LINE_LEN) {
                    g_line_itr++;
                } else if (g_local_echo) {
                    putchar('\b'); //Line full, overwrite last char.
                }

                if (g_local_echo) {
                    putchar(new_char);
                }
                break;

        } //endswitch
    } //end if valid new_char
}

static void parse_input(void)
{
    char* tok_ptr = 0;

    tok_ptr = strtok(g_cmd_line, WHITESPACE);

    while (tok_ptr) {
        if (g_argc < MAX_ARGS) {
            g_argv[g_argc++] = tok_ptr;
        } else {
            g_argc = 0;
            printf("Error: Too many arguments.\n");
            break;
        }

        tok_ptr = strtok(NULL, WHITESPACE);

    } //endwhile

    g_state = SHELL_RUN;
}

static void process_cmd(void)
{
    uint8_t cmd_itr = 0;

    //If no command found in parsing, print prompt and exit.
    if (g_argc == 0) {
        putchar('\n');
        print_prompt();
        g_state = SHELL_INPUT;
        return;
    }

    putchar('\n'); // Start function output on newline

    //Search builtin commands for match.
    for (; cmd_itr < BUILTIN_CMD_COUNT; cmd_itr++) {
        if (strncmp(g_argv[0], 
            g_builtin_cmds[cmd_itr].desc, 
            MAX_LINE_LEN) == 0) {

            g_builtin_cmds[cmd_itr].handler(g_argc, g_argv);
            g_argc = 0;
            break;
        }

    }

    //Search runtime registered commands for match.
    for (cmd_itr = 0; cmd_itr < g_registered_cmd_cnt; cmd_itr++) {
        if (strncmp(g_argv[0], 
            g_registered_cmds[cmd_itr].desc,
            MAX_LINE_LEN) == 0) {

            g_registered_cmds[cmd_itr].handler(g_argc, g_argv);
            g_argc = 0;
            break;
        }

    }

    //If no match found, list available commands.
    if (g_argc != 0) {
        g_argc = 0;
        print_help();
    }
    print_prompt();
    g_state = SHELL_INPUT;
}

static void print_help(void)
{
    uint8_t cmd_itr = 0;

    //List the builtin commands.
    for (; cmd_itr < BUILTIN_CMD_COUNT; cmd_itr++) {
        printf("%s\n", g_builtin_cmds[cmd_itr].desc);
    }

    //List the runtime registered commands.
    for (cmd_itr = 0; cmd_itr < g_registered_cmd_cnt; cmd_itr++) {
        printf("%s\n", g_registered_cmds[cmd_itr].desc);
    }
}

static void print_prompt(void)
{
    printf("%c ", g_shell_prompt);
}