/* Copyright (c) 2016 Trevor Wilcox wilcoxtw@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include <stdio.h>
#include <stdbool.h>
#include <termios.h>
#include <unistd.h>
#include "shell.h"

static bool g_quit = false;

static void disable_terminal_canonical_and_echo(void)
{
    struct termios term;

    tcgetattr(STDIN_FILENO, &term);
    term.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &term);
}

static void enable_terminal_cannonical_and_echo(void)
{
    struct termios term;

    tcgetattr(STDIN_FILENO, &term);
    term.c_lflag |= (ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &term);
}

static void print_func_call(const char* name, uint8_t argc, char* argv[])
{
    printf("%s called with argc:%d argv:", name, argc);
    for (int i = 0; i < argc; i++) {
        printf("%s ", argv[i]);
    }

    putchar('\n');
}

static void quit_cmd(uint8_t argc, char* argv[])
{
    g_quit = true;

    print_func_call(__FUNCTION__, argc, argv);
}

static void test_cmd1(uint8_t argc, char* argv[])
{
    print_func_call(__FUNCTION__, argc, argv);
}

static void test_cmd2(uint8_t argc, char* argv[])
{
    print_func_call(__FUNCTION__, argc, argv);
}

static void test_cmd3(uint8_t argc, char* argv[])
{
    print_func_call(__FUNCTION__, argc, argv);
}

static void test_cmd4(uint8_t argc, char* argv[])
{
    print_func_call(__FUNCTION__, argc, argv);
}

#define TEST_NUM_CMDS 5
shell_cmd_t test_cmds[TEST_NUM_CMDS] = {
    {
        "quit",
        quit_cmd
    },
    {
        "test_cmd1",
        test_cmd1,
    },
    {
        "test_cmd2",
        test_cmd2,
    },
    {
        "test_cmd3",
        test_cmd3,
    },
    {
        "test_cmd4",
        test_cmd4,
    }
};

int main(void)
{
    int ret_val = -1;
    int i = 0;

    disable_terminal_canonical_and_echo(); //OSX Terminal Specific.

    shell_init();

    for (; i < TEST_NUM_CMDS; i++) {
        ret_val = shell_register_cmd(test_cmds[i]);
        if (ret_val != 0) {
            printf(
                "Error: shell_register_cmd returned %d (%s)\n",
                ret_val,
                test_cmds[i].desc);
        }
    }

    while (!g_quit) {

        shell_service();

    }

    enable_terminal_cannonical_and_echo(); //OSX Terminal Specific.

    return 0;
}